# README #

Deploy and update BioViz.org and maven.bioviz.org content and applications using Ansible.

* * *

## Step 1: Create an Ubuntu host (unless you already have one) 

If a host already exists for your use, you can skip this step. 

In the Loraine Lab, we're mainly using AWS resources to create hosts and deploy software.
If you need to create a host using AWS, you can use the playbook `aws.yml`.

To use the playbook `aws.yml` and create a BioViz host in your AWS account:

* Create aws_vars.yml (see `example_aws_vars.yml`)
* Run `ansible-playbook aws.yml`

* * *

## Step 2: Configure the host and deploy the bioviz software on it

Once you have a host (see previous step), do the following:

* Create an inventory file (see `example_inventory.ini`)
* Create setup_vars.yml (see `example_setup_vars.yml`)
* Run `ansible-playbook -i [ your inventory file ] setup.yml`

Variables you should define in your inventory file;

* ansible_host - IP address of the target host
* domain - domain of the main Bioviz site on the host (see bioviz.org for an example)
* stack - Prod or Develop (required for deploying developer computer public keys)
* igb_branch -release IGB branch name (e.g., release-9.1.10)
* igb_version - released IGB version (e.g., 9.1.10)
* igb_major_version - first part of version number (e.g., 9 for igb_version 9.1.10)
* igb_repository - bitbucket repository address for igb_version released on the site (e.g., https://bitbucket.org/lorainelab/integrated-genome-browser)
* bioviz_repo - repository with bioviz content (e.g, https://bitbucket.org/lorainelab/bioviz.git)
* bioviz_version - bioiz repository branch to deploy (e.g., main)
* hub_facade_domain - domain for the hub facade back end code (e.g, translate.bioviz.org)
* maven_domain - domain for the maven repository content (e.g., maven.bioviz.org)

* * *

## Step 3: Test the site

If your site's domain name is not yet registered with a DNS server,
you can test it by "tricking" your browser and your host into thinking
that its domain name is whatever you want.

To do that:

* Edit your local /etc/hosts file to associate the domain name with the host IP address
* Modify the hosts /etc/hosts file in the same way 

* * *

## Note for Loraine Lab developers ##

Submit a pull request to add your public rsa key to `roles/pubkey/files/Develop/Add`

* * *

### Who do I talk to for help? ###

* Ann Loraine aloraine@uncc.edu
